plugins {
    kotlin("jvm") version "1.9.22"
    application
}

version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("com.jessecorbett:diskord-bot:5.4.1")
    implementation("com.github.kittinunf.fuel:fuel:2.3.0")

    testImplementation(kotlin("test"))
}

tasks {
    jar {
        manifest.attributes["Main-Class"] = "com.blackbook.diebot.MainKt"
        from(configurations.runtimeClasspath.get().map {
            if (it.isDirectory) it else zipTree(it)
        })
        duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    }
}


kotlin {
    jvmToolchain(11)
}

application {
    mainClass.set("com.blackbook.diebot.MainKt")
}
