package com.blackbook.diebot.domain

import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class DieParserShould {
    private val acceptedMessages = sequenceOf(
        "die",
        "die die",
        "die die die",
        "die die die die die die die die die die die die",
        "dIe",
        "dIe dIe",
        "dIe DIE die diE",
        "dIe DIE die diE ",
        "dIe DIE die diE  ",
        " dIe DIE die diE ",
        "  dIe DIE die diE ",
        "  dIe DIE die diE  ",
        "dIe,diE",
        "dIe , diE",
        "dIe .., diE",
        "dIe-diE",
        "dIe, DIE-die-diE",
        "dIe,;;.. DIE-die-diE",
        "dIe,;;.. DIE:die:diE",
        "die, diE",
        "DIE.die",
        "dIe DIE.die, diE",
        "die"
    )

    val rejectedMessages = sequenceOf(
        "diee",
        "diedie",
        "live",
        "die live",
        "dIedIe",
        "::..,,.,",
        "",
        "."
    )

    @Test
    fun acceptMessages() {
        acceptedMessages
            .map {
                println(it)
                parse(it)
            }
            .forEach { assertTrue(it) }
    }

    @Test
    fun rejectMessages() {
        rejectedMessages
            .map {
                println(it)
                parse(it)
            }
            .forEach { assertFalse(it) }
    }
}

