package com.blackbook.diebot

import com.blackbook.diebot.domain.parse
import com.jessecorbett.diskord.api.channel.ChannelClient
import com.jessecorbett.diskord.api.common.NamedChannel
import com.jessecorbett.diskord.bot.bot
import com.jessecorbett.diskord.bot.events
import com.jessecorbett.diskord.util.isFromUser
import java.lang.System.getenv

private val BOT_TOKEN = getenv("BOT_TOKEN")

suspend fun main() {
    bot(BOT_TOKEN) {
        events {
            onMessageCreate {
                if (it.isFromUser && isInDieChannel(it.channel)) {
                    if (parse(it.content)) {
                        it.reply("die")
                        println("die")
                    } else {
                        println("un down")
                        it.react("\uD83C\uDD98")
                        it.react("⬇")
                    }
                }
            }
        }
    }
}

private suspend fun isInDieChannel(channel: ChannelClient) =
    channel.getChannel()
        .let { it is NamedChannel && it.name.endsWith("die") }
