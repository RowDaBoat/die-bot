package com.blackbook.diebot.domain


const val justDie = """[dD][iI][eE]"""
const val separator = """[\s.,;:!?\\-]"""
const val allDie = "^($justDie($separator)+)*$justDie($separator)*\$"
val dieRegex = allDie.toRegex()

fun parse(message: String) =
    dieRegex.matches(message.trim())
