FROM debian:buster as BUILD

RUN apt-get update && apt-get install -y openjdk-11-jdk-headless gradle

WORKDIR /service
RUN gradle wrapper --no-daemon --gradle-version 6.6.1
RUN ./gradlew --no-daemon -m build
COPY settings.gradle.kts gradle.properties /service/
COPY service/ /service/
RUN ./gradlew --no-daemon build

FROM debian:buster
RUN apt-get update && apt-get install -y openjdk-11-jre-headless
COPY --from=BUILD /service/build/libs/*.jar .
CMD java -jar *.jar
